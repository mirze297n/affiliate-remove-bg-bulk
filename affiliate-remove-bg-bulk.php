<?php
/**
 *  Plugin Name: Affiliate remove bg bulk
 *  Version: 1.0.0
 *  Author: Eppeg software
 *  Description: ...
 */

define('ARBB_PLUGIN_URL',plugin_dir_url(__FILE__));
define('ARBB_PLUGIN_PATH',plugin_dir_path(__FILE__));

use Eppeg\App;

spl_autoload_register( function ( $class_name ) {
    if ( false !== strpos( $class_name, 'Eppeg\\' ) ) {
        $class_file = str_replace( 'Eppeg\\', '', $class_name ) . '.class.php';
        $class_file = str_replace( '\\', DIRECTORY_SEPARATOR, $class_file );
        $class_file_path = ARBB_PLUGIN_PATH . 'includes' . DIRECTORY_SEPARATOR . $class_file;
        if (file_exists($class_file_path)) {
            require_once( $class_file_path );
        }
    }
});

App::run();
