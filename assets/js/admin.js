jQuery(document).ready(function ($) {

    let startkey = 0;
    let paged = 1;
    let successmessage = '<div class="arbb-info-container"><div class="image-name-info"><p>{url}</p></div> <div class="image-response-success-info"><span>Success</span></div></div>';
    let failmessage = '<div class="arbb-info-container"><div class="image-name-info"><p>{url}</p></div> <div class="image-response-fail-info"><span>Fail</span></div></div>';
    let alreadymessage = '<div class="arbb-info-container"><div class="image-name-info"><p>{url}</p></div> <div class="image-response-already-info"><span>Already processed</span></div></div>';

    $('.Start-ARBB-work').on('click', function () {
        let btn = $(this);
        let post_type = $('.arbb-post-types').val();
        btn.attr('disabled', 'disabled');
        btn.html('Loading...');
        get_all_attached_images(post_type);

        $('.arbb-post-types').remove();
        $('.Start-ARBB-work').html('Processing...');
    });

    function get_all_attached_images(post_type) {
        $.ajax({
            type: 'post',
            url: ajax_script.ajaxurl,
            dataType: 'JSON',
            data: {
                action: 'get_all_attached_imgs',
                paged: paged,
                post_type: post_type,
            },
            success: function (response) {
                images = response.images;

                if (images.length == 0) {
                    $('.Start-ARBB-work').html('All images processed');
                    return;
                }

                sendimagetoAPI(images);
            }
        });
    }

    function sendimagetoAPI(images) {
        $.ajax({
            type: 'post',
            url: ajax_script.ajaxurl,
            dataType: 'JSON',
            data: {
                action: 'send_to_api',
                image: images[startkey].url,
                id: images[startkey].id,
                post_id: images[startkey].post_id
            },
            success: function (response) {

                if ($('.arbb-response-container').children().length == 500) {
                    $('.arbb-response-container').html('');
                }

                if (response.status == 1) {
                    $('.arbb-response-container').prepend(successmessage.replace('{url}', response.url));
                } 
                else if (response.status == 2) {
                    $('.arbb-response-container').prepend(alreadymessage.replace('{url}', response.url));
                }
                else if (response.status == 3) {
                    $('.arbb-response-container').prepend(failmessage.replace('{url}', response.url));
                }

                if (images.length - 1 == startkey) {
                    paged++;
                    startkey = 0;
                    get_all_attached_images();
                    return;
                }
                
                startkey++;
                sendimagetoAPI(images);
            }
        })
    }

});