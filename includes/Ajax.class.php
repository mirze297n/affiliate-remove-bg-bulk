<?php
namespace Eppeg;


class Ajax{

     public static function get_all_attached_imgs(){
        $paged = $_POST['paged'];
        $post_type = $_POST['post_type'];
        $attachments = App::get_post_attachments($paged,$post_type);
        echo json_encode(['images' => $attachments,'count' => count($attachments)]);
        wp_die();
    }
    
    public static function send_to_api(){
         $url = $_POST['image'];
         $attachment_id = $_POST['id'];
         $post_id = $_POST['post_id'];
         $remove_result = App::process_remove_bg($attachment_id, $url, $post_id);
         echo json_encode(['status'=> $remove_result, 'url'=>'url:' . $url ,'post_id' => $post_id]);
         wp_die();
    }
}