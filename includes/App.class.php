<?php

namespace Eppeg;

class App
{

    const BG_SUCCESS = 1;
    const BG_ALREADY = 2;
    const BG_FAIL = 3;

    static $logger;

    static function run()
    {
        self::$logger = Logger::instance();
        add_action('admin_enqueue_scripts', ['\Eppeg\App', 'enqueue_scripts_styles']);
        add_action('admin_menu', ['\Eppeg\App', 'add_admin_menu']);
        add_action('wp_ajax_get_all_attached_imgs', ['\Eppeg\Ajax', 'get_all_attached_imgs']);
        add_action('wp_ajax_send_to_api', ['\Eppeg\Ajax', 'send_to_api']);
    }

    static function enqueue_scripts_styles()
    {
        wp_enqueue_style('ARBB_admin_style', ARBB_PLUGIN_URL . 'assets/css/admin.css?t=' . time());
        wp_enqueue_script('ARBB_admin_script', ARBB_PLUGIN_URL . 'assets/js/admin.js?t=' . time(), ['jquery']);
        wp_localize_script('ARBB_admin_script', 'ajax_script', ['ajaxurl' => admin_url('admin-ajax.php')]);
    }

    static function add_admin_menu()
    {
        add_menu_page('Remove BG Bulk', 'Remove BG Bulk', 'adminstrator', 'ARBB_page', ['\Eppeg\App', 'admin_menu_html'], 'dashicons-admin-tools', 26);
        add_submenu_page('ARBB_page', 'Bulk run', 'Bulk run', 'administrator', 'ARBB_subpage', ['\Eppeg\App', 'admin_menu_html'], 26);
    }

    function admin_menu_html() {
        Utils::renderTemplate('ARBB-admin-page');
    }

    static function get_post_attachments($paged, $post_type)
    {
        $args = array(
            'post_type' => $post_type,
            'post_status' => 'publish',
            'paged' => $paged,
            'posts_per_page' => 100,
        );

        $posts = get_posts($args);
        $attachments = [];

        foreach ($posts as $post) {
            $info_shortcode_content = self::extract_shortcodes_from_content($post->post_content, 'info');
            $images = self::extract_images_from_content($info_shortcode_content);

            foreach ($images as $url) {
                $attachments[] = [
                    'id' => attachment_url_to_postid($url),
                    'post_id' => $post->ID,
                    'url' => $url,
                ];
            }
        }

        return $attachments;
    }

    static function extract_shortcodes_from_content($content, $shortcode) {
        $regex = '#\[' . $shortcode . '[^\[]+\[/' . $shortcode . ']#';
        preg_match_all($regex, $content, $matches);
        return implode(' ', $matches[0]);
    }

    static function extract_images_from_content($content) {
        $regex = '/src="([^"]*)"/';
        preg_match_all($regex, $content, $matches);
        return array_unique(preg_replace(['#src=#', '#"#', '#-[0-9]+x[0-9]+\.#'], ['', '', '.'], $matches[0]));
    }

    static public function process_remove_bg($attachment_id, $old_img_url, $post_id)
    {
        global $wpdb;

        self::$logger
            ->setType('INFO')
            ->add("New request -> attach_id: $attachment_id, old_url: $old_img_url. post_id: $post_id");

        if (get_post_meta($attachment_id, '_processed_removebg', true) == 1) {
            // update post content
            // self::update_post_content($post_id, $old_img_url, null, $attachment_id);
            self::$logger
                ->setType('INFO')
                ->add("Allready processed");

            return self::BG_ALREADY;
        }

        $wp_upload_dir = wp_upload_dir();

        // get abs path of attachment
        $old_file_path = get_attached_file($attachment_id);

        // send request to the remove.bg api with image url
        $api_response = wp_remote_post(
            'https://api.remove.bg/v1.0/removebg',
            [
                'headers' => [
                    'X-Api-Key' => 'L3UDTetY9gbGtH5TrBHpJcpd',
                ],
                'body' => [
                    'image_url' => $old_img_url,
                    'size' => 'auto'
                ]
            ]
        );

        // api response is erronous, then return false
        if (is_wp_error($api_response) || $api_response['response']['code'] != 200) {

            self::$logger
                ->setType('ERROR')
                ->add("Api response: " . serialize($api_response['body']));

            return self::BG_FAIL;
        }

        // image data with bg removed
        $image_unique_id = uniqid();
        $bg_image = $api_response['body'];
        $old_file_pathinfo = pathinfo($old_file_path);
        $new_file_path = $old_file_pathinfo['dirname'] . '/' . $image_unique_id . '-' . $old_file_pathinfo['filename'] . '.png';

        self::$logger
            ->setType('INFO')
            ->add("New file path: $new_file_path");

        // create new file with png extension, and owerwrite its content with image from api
        $fp = fopen($new_file_path, "wb");
        fwrite($fp, $bg_image);
        fclose($fp);

        $old_img_url_info = pathinfo($old_img_url);
        $new_file_url = $old_img_url_info['dirname'] . '/' . $image_unique_id . '-' . $old_img_url_info['filename'] . '.png';

        self::$logger
            ->setType('INFO')
            ->add("New file url: $new_file_url");

        // generate attachment metadata based on new image
        $attachment_data = wp_generate_attachment_metadata($attachment_id, $new_file_path);
        wp_update_attachment_metadata($attachment_id, $attachment_data);

        // mark image as procssed
        update_post_meta($attachment_id, '_processed_removebg', 1);

        // update some data in wp due to possibily image extension change
        update_post_meta($attachment_id, '_wp_attached_file', str_replace($wp_upload_dir['basedir'] . '/', '', $new_file_path));
        $wpdb->update(
            "{$wpdb->prefix}posts",
            ['guid' => $new_file_url, 'post_mime_type' => 'image/png'],
            ['ID' => $attachment_id]
        );

        // update post content
        self::update_post_content($post_id, $old_img_url, $new_file_url, null);

        // remove old images
        $files_to_delete = $old_file_pathinfo['dirname'] . '/' . $old_file_pathinfo['filename'] . '*';
        foreach (glob($files_to_delete) as $file_to_delete) {
            @unlink($file_to_delete);

            self::$logger
                ->setType('INFO')
                ->add("Deleting old file: $file_to_delete");
        }

        return self::BG_SUCCESS;

    } // end of function


    function update_post_content($post_id, $old_image_url, $new_image_url, $attachment_id)
    {
        self::$logger
            ->setType('INFO')
            ->add("Updateing post content -> post_id: $post_id, old_image_url: $old_image_url, new_image_url: $new_image_url, attachment_id: $attachment_id");
        
        if (is_null($new_image_url)) {
            $new_image_url = wp_get_attachment_image_url($attachment_id, 'full');
        }

        $post = get_post($post_id);
        $content = $post->post_content;
        $pathinfo = pathinfo($old_image_url);

        $regex = '#' . $pathinfo['dirname'] . '/' . $pathinfo['filename'] . '[^"]*' . '\.' . $pathinfo['extension'] . '#';

        $content = preg_replace($regex, $new_image_url, $content);
        $updated_post = [
            'ID' => $post_id,
            'post_content' => $content
        ];
        wp_update_post($updated_post);
    }

} // end of class