<?php

namespace Eppeg;

class Logger {
    
    private $_dest;
    private $_type;

    static function instance() {
        $instance = new self();
        $log_path = wp_upload_dir()['basedir'] . '/bgremover-bulk/';
        if (!file_exists($log_path)) {
            mkdir($log_path);
            $log_file = fopen($log_path . 'general.log', "w");
            fclose($log_file);
        }
        $instance->_dest = $log_path . 'general.log';
        return $instance;
    }


    function add($text) {
        if (is_array($text) || is_object($text)) {
            $text = serialize($text);
        }

        $datetime = date('Y-m-j H:i:s');
        $fp = fopen($this->_dest, 'a');
        fwrite($fp, "$this->_type [$datetime] : $text \n");
        fclose($fp);

        return $this;
    }

    function setType($type) {
        $this->_type = $type;
        return $this;
    }
}