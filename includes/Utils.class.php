<?php

namespace Eppeg;

class Utils
{

    static function renderTemplate( $name, $data = [], $once = false ) {
        $file = ARBB_PLUGIN_PATH . 'templates' . DIRECTORY_SEPARATOR . $name . '.php';
        if ( $once ) {
            require_once( $file );
        } else {
            require( $file );
        }
    }

    static function getTemplate( $name, $data = [], $once = false ) {
        ob_start();
        self::renderTemplate( $name, $data, $once );
        return ob_get_clean();
    }

    static function printArray($arr) {
        echo '<pre>';
        print_r($arr);
        echo '</pre>';
    }
}
