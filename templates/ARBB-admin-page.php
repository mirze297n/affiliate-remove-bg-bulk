<?php
if (isset($data)){
    extract($data);
}
$post_types = get_post_types();
?>

<div class="arbb-container">
    <div class="arbb-select-container">
        <select class="arbb-post-types">
            <option>Select post type</option>
            <?php

                foreach ($post_types as $post_type):
                    $selected = '';
                    if($post_type == 'post'){
                        $selected = 'selected';
                    }
                    echo '<option value="'.$post_type.'" '. $selected .'>'.$post_type.'</option>';
                endforeach;
            ?>
        </select>
    </div>
    <div class="arbb-button-container">
        <button class="Start-ARBB-work">Start</button>
    </div>
    <div class="arbb-response-container"></div>

</div>

<?php




